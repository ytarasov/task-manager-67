package ru.t1.ytarasov.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class TaskDto extends AbstractWbsModelDto {

    @Nullable
    private String projectId;

    public TaskDto(@NotNull String name, @NotNull String description) {
        super(name, description);
    }

}
