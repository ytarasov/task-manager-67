package ru.t1.ytarasov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.ytarasov.tm.configuration.ApplicationConfiguration;
import ru.t1.ytarasov.tm.configuration.WebConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class
        };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebConfiguration.class
        };
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

}
