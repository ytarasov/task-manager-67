package ru.t1.ytarasov.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractWbsModelDto extends AbstractModelDto {

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String description;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @Column(name = "date_start")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Column(name = "date_finish")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateFinish;

    public AbstractWbsModelDto(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
