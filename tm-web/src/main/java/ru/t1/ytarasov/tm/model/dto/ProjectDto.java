package ru.t1.ytarasov.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class ProjectDto extends AbstractWbsModelDto {

    public ProjectDto(@NotNull String name, @NotNull String description) {
        super(name, description);
    }

}
