package ru.t1.ytarasov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.event.ConsoleEvent;

public interface IListener {

    void handler(@NotNull final ConsoleEvent event) throws Exception;

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

}
