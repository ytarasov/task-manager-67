package ru.t1.ytarasov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-change-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change task by id.";

    @Override
    @EventListener(condition = "@taskChangeStatusByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[CHANGE STATUS OF THE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setTaskId(id);
        request.setStatus(status);
        getTaskEndpoint().changeTaskStatusById(request);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
