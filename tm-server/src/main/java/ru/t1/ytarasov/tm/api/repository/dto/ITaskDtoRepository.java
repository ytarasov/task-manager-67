package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Modifying
    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    @Query("SELECT t FROM TaskDTO t ORDER BY :sortType")
    List<TaskDTO> findAllWithSort(@NotNull @Param("sortType") final String sortType);

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sortType")
    List<TaskDTO> findAllWithUserIdAndSort(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("sortType") final String sortType
    );

    @Nullable
    List<TaskDTO> findByUserId(@NotNull final String userId);

    @Nullable
    TaskDTO findByIdAndUserId(@NotNull final String id, @NotNull final String userId);

    Long countByUserId(@NotNull final String id);

    Boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
